import { Component, OnInit } from '@angular/core';
import { MainService } from 'src/app/main.service';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';

@Component({
  selector: 'app-request',
  templateUrl: './request.component.html',
  styleUrls: ['./request.component.css']
})
export class RequestComponent implements OnInit {
  b: boolean = true
  accept: any[]
  loggedUser;
  user;
  constructor(private service: MainService, private http: HttpClient, private router: Router) { }
  ngOnInit() {
    this.user = this.service.sendLoggedUser()
    this.service.viewSpecificrequest(this.user).subscribe(accept => {
      if (accept['message'] == 'unauthorized access') {
        alert(accept['message'])
        this.router.navigate(['/home/login'])
      }
      else {
        this.accept = accept['message']
      }
    })
  }
  requestof(x) {
    this.loggedUser = this.service.sendLoggedUser();
    x.rollno = this.loggedUser.rollno;
    this.http.post('/student/saverequest', x).subscribe(res => {
      alert(res["message"])
      this.user = this.service.sendLoggedUser()
      this.service.viewSpecificrequest(this.user).subscribe(accept => {
        this.accept = accept['message']
      })
    })
    this.b = false
  }
}
