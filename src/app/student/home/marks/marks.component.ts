import { Component, OnInit } from '@angular/core';
import { MainService } from 'src/app/main.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-marks',
  templateUrl: './marks.component.html',
  styleUrls: ['./marks.component.css']
})
export class MarksComponent implements OnInit {

  constructor(private service: MainService, private router: Router) { }
  marks: any[] = []
  user;

  ngOnInit() {
    this.user = this.service.sendLoggedUser()
    this.service.viewSpecificmarks(this.user).subscribe(marks => {
      if (marks['message'] == "unauthorized access") {
        alert(marks['message'])
        this.router.navigate(['/home/login'])
      }
      else {
        this.marks = marks['message']
      }
    })
  }
}