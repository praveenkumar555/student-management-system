import { Component, OnInit } from '@angular/core';
import { MainService } from 'src/app/main.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-notifications',
  templateUrl: './notifications.component.html',
  styleUrls: ['./notifications.component.css']
})
export class NotificationsComponent implements OnInit {
  note: any[] = []
  constructor(private service: MainService, private router: Router) { }

  ngOnInit() {
    this.service.readnote().subscribe(note => {
      if (note['message'] == "unauthorized access") {
        alert(note['message'])
        this.router.navigate(['/home/login'])
      }
      else {
        this.note = note['message']
      }
    })
  }

}
