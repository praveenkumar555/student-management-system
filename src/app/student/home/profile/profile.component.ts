import { Component, OnInit } from '@angular/core';
import { MainService } from 'src/app/main.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {

  constructor(private service: MainService, private router: Router) { }
  profile: any[] = []
  user;

  ngOnInit() {
    this.user = this.service.sendLoggedUser()
    this.service.viewspecificstudentdetails(this.user).subscribe(profile => {
      if (profile['message'] == 'unauthorized access') {
        alert(profile['message'])
        this.router.navigate(['/home/login'])
      }
      else {
        this.profile = profile['message']
      }
    })
  }
}
