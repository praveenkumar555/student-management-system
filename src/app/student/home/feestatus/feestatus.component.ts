import { Component, OnInit } from '@angular/core';
import { MainService } from 'src/app/main.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-feestatus',
  templateUrl: './feestatus.component.html',
  styleUrls: ['./feestatus.component.css']
})
export class FeestatusComponent implements OnInit {

  constructor(private service: MainService, private router: Router) { }
  fee: any[] = []
  user;
  ngOnInit() {
    this.user = this.service.sendLoggedUser()
    this.service.viewspecificfeestatus(this.user).subscribe(fee => {
      if (fee['message'] == "unauthorized access") {
        alert(fee['message'])
        this.router.navigate(['/home/login'])
      }
      else {
        this.fee = fee['message']
      }
    })

  }

}
