import { Component, OnInit } from '@angular/core';
import { MainService } from 'src/app/main.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-attendance',
  templateUrl: './attendance.component.html',
  styleUrls: ['./attendance.component.css']
})
export class AttendanceComponent implements OnInit {

  constructor(private service: MainService, private router: Router) { }
  attend: any[] = []
  user;
  ngOnInit() {
    this.user = this.service.sendLoggedUser()
    this.service.viewspecificattendance(this.user).subscribe(attend => {
      if (attend['message'] == "unauthorized access") {
        alert(attend['message'])
        this.router.navigate(['/home/login'])
      }
      else {
        this.attend = attend['message']
      }
    })
  }
}
