import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { StudentRoutingModule } from './student-routing.module';
import { HomeComponent } from './home/home.component';
import { MarksComponent } from './home/marks/marks.component';
import { AttendanceComponent } from './home/attendance/attendance.component';
import { FeestatusComponent } from './home/feestatus/feestatus.component';
import { NotificationsComponent } from './home/notifications/notifications.component';
import { LogoutComponent } from './home/logout/logout.component';
import { ProfileComponent } from './home/profile/profile.component';
import { RequestComponent } from './home/request/request.component';
import { FormsModule } from '@angular/forms'

@NgModule({
  declarations: [HomeComponent, MarksComponent, AttendanceComponent, FeestatusComponent, NotificationsComponent, LogoutComponent, ProfileComponent, RequestComponent],
  imports: [
    CommonModule,
    StudentRoutingModule,
    FormsModule
  ]
})
export class StudentModule { }
