import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class MainService {
user;
  constructor(private http:HttpClient) { }
  
readstd():Observable<any[]>
{
  return this.http.get<any[]>('admin/readstd')
}
readatt():Observable<any[]>
{
  return this.http.get<any[]>('student/readatt')
}
readfee():Observable<any[]>
{
  return this.http.get<any[]>('student/readfee')
}
readmark():Observable<any[]>
{
  return this.http.get<any[]>('student/readmark')
}
readnote():Observable<any[]>
{
  return this.http.get<any[]>('student/readnote')
}
readaccept():Observable<any[]>
{
  return this.http.get<any[]>('student/readaccept')
}
loggedUser(user){

  this.user=user[0]
}
sendLoggedUser()
{
  return this.user;
}
viewSpecificmarks(user){
  return this.http.post<any[]>('student/viewSpecificmarks',user)
}
viewspecificattendance(user){
  return this.http.post<any[]>('student/viewspecificattendance',user)
}
viewspecificfeestatus(user){
  return this.http.post<any[]>('student/viewspecificfeestatus',user)
}
viewspecificstudentdetails(user){
  return this.http.post<any[]>('student/viewspecificstudentdetails',user)
}
viewSpecificrequest(user){
  return this.http.post<any[]>('student/viewspecificrequest',user)
}
}