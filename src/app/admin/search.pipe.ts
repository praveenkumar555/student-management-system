import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'search'
})
export class SearchPipe implements PipeTransform {

  transform(admin: any[], searchterm: any): any {
    if (!searchterm) {
      return admin
    }
    return admin.filter(rollno => rollno.rollno.toString().indexOf(searchterm.toLowerCase()) !== -1);
  }

}
