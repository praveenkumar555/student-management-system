import { NgModule, Component } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { AttendanceComponent } from './home/attendance/attendance.component';
import { FeestatusComponent } from './home/feestatus/feestatus.component';
import { LogoutComponent } from './home/logout/logout.component';
import { MarksComponent } from './home/marks/marks.component';
import { NotificationsComponent } from './home/notifications/notifications.component';
import { RequestComponent } from './home/request/request.component';
import { AddstudentComponent } from './home/addstudent/addstudent.component';
import { StudentprofileComponent } from './home/studentprofile/studentprofile.component';

const routes: Routes = [
  {
    path: '',
    component: HomeComponent,
    children: [
      {
        path: 'attendance',
        component: AttendanceComponent,
      },
      {
        path: 'feestatus',
        component: FeestatusComponent,
      },
      {
        path: 'logout',
        component: LogoutComponent,
      },
      {
        path: 'marks',
        component: MarksComponent,
      },
      {
        path: 'notifications',
        component: NotificationsComponent,
      },
      {
        path: 'request',
        component: RequestComponent,
      },
      {
        path: 'addstudent',
        component: AddstudentComponent
      },
      {
        path: 'studentprofile',
        component: StudentprofileComponent
      }
    ]
  },

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }
