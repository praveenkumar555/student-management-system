import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdminRoutingModule } from './admin-routing.module';
import { HomeComponent } from './home/home.component';
import { MarksComponent } from './home/marks/marks.component';
import { AttendanceComponent } from './home/attendance/attendance.component';
import { FeestatusComponent } from './home/feestatus/feestatus.component';
import { NotificationsComponent } from './home/notifications/notifications.component';
import { LogoutComponent } from './home/logout/logout.component';
import { FormsModule } from '@angular/forms';
import { RequestComponent } from './home/request/request.component';
import { AddstudentComponent } from './home/addstudent/addstudent.component';
import { StudentprofileComponent } from './home/studentprofile/studentprofile.component';
import { SearchPipe } from './search.pipe';


@NgModule({
  declarations: [HomeComponent, MarksComponent, AttendanceComponent, FeestatusComponent, NotificationsComponent, LogoutComponent, RequestComponent, AddstudentComponent, StudentprofileComponent, SearchPipe],
  imports: [
    CommonModule,
    AdminRoutingModule,
    FormsModule,
  ]
})
export class AdminModule { }
