import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-addstudent',
  templateUrl: './addstudent.component.html',
  styleUrls: ['./addstudent.component.css']
})
export class AddstudentComponent implements OnInit {

  constructor(private httpclient: HttpClient) { }

  ngOnInit() {
  }
  addstudents(x) {
    if (x.rollno == "" || x.name == "" || x.password == "" || x.class == "" || x.gmail == "" || x.phone == "") {
      alert("enter the valid data")
    }
    else {
      this.httpclient.post('/admin/savestd', x).subscribe((res) => {
        alert(res['message'])
      })
    }
  }

}
