import { Component, OnInit } from '@angular/core';
import { MainService } from 'src/app/main.service';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router'

@Component({
  selector: 'app-attendance',
  templateUrl: './attendance.component.html',
  styleUrls: ['./attendance.component.css']
})
export class AttendanceComponent implements OnInit {
  constructor(private http: HttpClient, private service: MainService, private router: Router) { }
  attend: any[] = []
  b: boolean = false
  objecttomodify: object
  searchterm: any
  ngOnInit() {
    this.service.readatt().subscribe(attend => {
      if (attend['message'] == "unauthorized access") {
        alert(attend['message'])
        this.router.navigate(['/home/login'])
      }
      else {
        this.attend = attend['message']
      }
    })
  }
  attendance(x) {
    if (x.rollno == "" || x.month == "" || x.overall == "") {
      alert("enter valid details")
    }
    else {
      this.http.post('/admin/saveatt', x).subscribe((res) => {
        alert(res['message'])
        this.service.readatt().subscribe(attend => {
          this.attend = attend['message']
        })
      })
    }
  }
  editdata(attendance) {
    this.objecttomodify = attendance;
    this.b = true;
  }
  onsubmit(modifyobject) {
    this.b = false
    this.http.put('/admin/modifyatt', modifyobject).subscribe((res) => {
      alert(res['message'])
    })
  }
  deleterecord(rollno) {
    this.http.delete(`/admin/deleteatt/${rollno}`).subscribe(res => {
      alert(res["message"]);
      this.attend = res['data']

    })
  }
}
