import { Component, OnInit } from '@angular/core';
import { MainService } from 'src/app/main.service';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';


@Component({
  selector: 'app-marks',
  templateUrl: './marks.component.html',
  styleUrls: ['./marks.component.css']
})
export class MarksComponent implements OnInit {
  marks: any[] = []
  b: boolean = false
  objecttomodify: object
  searchterm: any;
  constructor(private http: HttpClient, private service: MainService, private router: Router) { }

  ngOnInit() {
    this.service.readmark().subscribe(marks => {
      if (marks['message'] == "unauthorized access") {
        alert(marks['message'])
        this.router.navigate(['/home/login'])
      }
      else {
        this.marks = marks['message']
      }
    })
  }
  marksof(x) {
    if (x.rollno == "" || x.subject == "" || x.marks == "" || x.totalmarks == "" || x.grade == "") {
      alert("please enter the data")
    }
    else {
      this.http.post('/admin/savemark', x).subscribe((res) => {
        alert(res['message'])
        this.service.readmark().subscribe(marks => {
          this.marks = marks['message']
        })
      })

    }
  }
  deleterecord(rollno) {
    this.http.delete(`/admin/deletemark/${rollno}`).subscribe(res => {
      alert(res["message"]);
      this.marks = res['data']

    })
  }
  editdata(marks) {
    this.b = true;
    this.objecttomodify = marks;

  }
  onsubmit(modifyobject) {
    this.b = false
    this.http.put('/admin/modifymark', modifyobject).subscribe((res) => {
      alert(res['message'])
    })
  }

}
