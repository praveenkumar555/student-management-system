import { Component, OnInit } from '@angular/core';
import { MainService } from 'src/app/main.service';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';

@Component({
  selector: 'app-request',
  templateUrl: './request.component.html',
  styleUrls: ['./request.component.css']
})
export class RequestComponent implements OnInit {

  constructor(private service: MainService, private http: HttpClient, private router: Router) { }

  c: any
  d: any
  data: any[] = []
  loggedUser;
  b: boolean = false
  ngOnInit() {
    this.http.get('/admin/getrequest').subscribe(data => {
      if (data['message'] == "unauthorized access") {
        alert(data['message'])
        this.router.navigate(['/home/login'])
      }
      else {
        this.data = data['message']
      }
    })
  }
  accept(rollno) {
    this.loggedUser = this.service.sendLoggedUser();
    this.http.post('/admin/saveres', ({ "message": "request is accepted", "rollno": rollno })).subscribe((res) => {
      alert(res['message'])
      this.http.get('/admin/getrequest').subscribe(data => {
        this.data = data['message']
      })
    })
  }
  rejectbutton(rollno) {

  }



  reject(x, rollno) {
    this.loggedUser = this.service.sendLoggedUser();
    x.rollno = rollno
    x.message = "request is rejected"
    this.http.post('/admin/saveres', x).subscribe((res) => {
      alert(res['message'])
      this.http.get('/admin/getrequest').subscribe(data => {
        this.data = data['message']
      })
    })
    this.b = true
  }
}
