import { Component, OnInit } from '@angular/core';
import { MainService } from 'src/app/main.service';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';

@Component({
  selector: 'app-notifications',
  templateUrl: './notifications.component.html',
  styleUrls: ['./notifications.component.css']
})
export class NotificationsComponent implements OnInit {

  constructor(private http: HttpClient, private service: MainService, private router: Router) { }
  ngOnInit() {
  }
  sendnote(x) {
    this.http.post('/admin/savenote', x).subscribe((res) => {
      alert(res['message'])
      this.router.navigate(['/admin/studentprofile'])
    })
  }
}
