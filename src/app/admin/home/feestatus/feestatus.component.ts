import { Component, OnInit } from '@angular/core';
import { MainService } from 'src/app/main.service';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router'
@Component({
  selector: 'app-feestatus',
  templateUrl: './feestatus.component.html',
  styleUrls: ['./feestatus.component.css']
})
export class FeestatusComponent implements OnInit {

  constructor(private http: HttpClient, private service: MainService, private router: Router) { }
  fee: any[] = []
  objecttomodify: object
  b: boolean = false
  searchterm: any
  ngOnInit() {
    this.service.readfee().subscribe(fee => {
      if (fee['message'] == "unauthorized access") {
        alert(fee['message'])
        this.router.navigate(['/home/login'])
      }
      else {
        this.fee = fee['message']
      }
    })
  }
  feestatus(x) {
    if (x.rollno == "" || x.totalfee == "" || x.feepaid == "" || x.pending == "") {
      alert("enter valid details")
    }
    else {
      this.http.post('/admin/savefee', x).subscribe((res) => {
        alert(res['message'])
        this.service.readfee().subscribe(fee => {
          this.fee = fee['message']
        })
      })
    }
  }
  deleterecord(rollno) {
    this.http.delete(`/admin/deletefee/${rollno}`).subscribe(res => {
      alert(res["message"]);
      this.fee = res['data']

    })
  }
  editdata(fee) {
    this.b = true;
    this.objecttomodify = fee;
    console.log(this.objecttomodify)

  }
  onsubmit(modifyobject) {
    this.b = false
    this.http.put('/admin/modifymark', modifyobject).subscribe((res) => {
      alert(res['message'])
    })
  }

}
