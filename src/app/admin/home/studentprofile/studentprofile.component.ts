import { Component, OnInit } from '@angular/core';
import { MainService } from 'src/app/main.service';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';

@Component({
  selector: 'app-studentprofile',
  templateUrl: './studentprofile.component.html',
  styleUrls: ['./studentprofile.component.css']
})
export class StudentprofileComponent implements OnInit {

  constructor(private ds: MainService, private http: HttpClient, private router: Router) { }
  data: any[] = []
  searchterm: any
  ngOnInit() {
    this.ds.readstd().subscribe(data => {
      if (data['message'] == "unauthorized access") {
        alert(data['message'])
        this.router.navigate(['/home/login'])
      }
      else {
        this.data = data['data']
      }
    })
  }
  deleterecord(rollno) {
    this.http.delete(`/admin/deleteprofile/${rollno}`).subscribe(res => {
      alert(res["message"]);
      this.data = res['data']
    })
  }

}
