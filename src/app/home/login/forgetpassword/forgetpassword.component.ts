import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';

@Component({
  selector: 'app-forgetpassword',
  templateUrl: './forgetpassword.component.html',
  styleUrls: ['./forgetpassword.component.css']
})
export class ForgetpasswordComponent implements OnInit {

  constructor(private http: HttpClient, private router: Router) { }

  ngOnInit() {
  }
  password(x) {
    this.http.post('admin/forgetpassword', x).subscribe((res) => {
      alert(res['message'])
      if (res['message'] == "user found") {
        this.router.navigate(['/home/otp'])
      }
      else {
        this.router.navigate(['/home/forgetpassword'])
      }
    })
  }

}
