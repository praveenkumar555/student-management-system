import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';

@Component({
  selector: 'app-changepassword',
  templateUrl: './changepassword.component.html',
  styleUrls: ['./changepassword.component.css']
})
export class ChangepasswordComponent implements OnInit {

  constructor(private http: HttpClient, private router: Router) { }

  ngOnInit() {
  }
  changepassword(x) {
    this.http.put('/admin/changepassword', x).subscribe((res) => {
      alert(res['message'])
      if (res['message'] == 'password changed') {
        this.router.navigate(['home/login'])
      }
      else {
        this.router.navigate(['/home/changepassword'])
      }
    })
  }
}
