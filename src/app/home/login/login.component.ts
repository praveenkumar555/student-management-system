import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { LoginService } from 'src/app/login.service';
import { MainService } from 'src/app/main.service';



@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(private router: Router, private http: HttpClient, private loginservice: LoginService, private mainservice: MainService) { }

  ngOnInit() {
  }
  onSubmit(x) {
    if (x.rollno == "" || x.password == "") {
      alert("please enter the details")
    }
    else {
      if (x.user == "admin") {
        this.loginservice.dologinadmin(x).subscribe(res => {
          if (res["message"] === "invalid admin") {
            alert("invalid admin")
          }
          else if (res["message"] === "invalid admin password") {
            alert("wrong password enter correct password")
          }
          else if (res["message"] === "admin login success") {
            alert(res['message']);
            localStorage.setItem('idToken', res['token'])
            this.loginservice.isLoggedIn = true
            this.router.navigate(['/admin/studentprofile'])
          }
        })
      }
      else if (x.user == "student") {
        this.loginservice.dologin(x).subscribe(res => {
          if (res["message"] === "invalid student") {
            alert("Invalid user")
          }
          if (res["message"] === "invalid password") {
            alert("Password is not valid")
          }
          if (res["message"] === "student login success") {
            alert(res["message"])
            localStorage.setItem('idToken', res['token'])
            this.loginservice.isLoggedIn = true
            this.mainservice.loggedUser(res['data'])
            this.router.navigate(["/student/profile"]);
          }
        })

      }
    }
  }
}
