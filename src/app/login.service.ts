import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LoginService {
  isLoggedIn:boolean=true
  constructor(private http:HttpClient) { }
dologin(userobject):Observable<any>
{
  return this.http.post<any>('/admin/login',userobject)
}
dologinadmin(userobject):Observable<any>
{
  return this.http.post<any>('/admin/adminlogin',userobject)
}
}
