import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { HomepageComponent } from './home/homepage/homepage.component';
import { LoginComponent } from './home/login/login.component';
import { ForgetpasswordComponent } from './home/login/forgetpassword/forgetpassword.component';
import { OtpComponent } from './home/login/otp/otp.component';
import { ChangepasswordComponent } from './home/login/changepassword/changepassword.component';



const routes: Routes = [
  {
    path:'home',
    component:HomeComponent,
    children:[
      {
        path:'homepage',
        component:HomepageComponent,
      },
      {
        path:'login',
        component:LoginComponent,
      },
          {
            path:'forgetpassword',
            component:ForgetpasswordComponent
          },
          {
            path:'otp',
            component:OtpComponent
          },
          {
            path:'changepassword',
            component:ChangepasswordComponent
          }
    ]
  },
     
 
      
    
   {path:"admin",loadChildren:()=>import('./admin/admin.module').then(mod=>mod.AdminModule)},
   {path:"student",loadChildren:()=>import('./student/student.module').then(mod=>mod.StudentModule)},
   {path:"",redirectTo:"home/homepage",pathMatch:"full"}
    ]

@NgModule({
  imports: [RouterModule.forRoot(routes,{useHash:true})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
