import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { HomepageComponent } from './home/homepage/homepage.component';
import { LoginComponent } from './home/login/login.component';
import { FormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http'
import { AuthorizationService } from './authorization.service';
import { ForgetpasswordComponent } from './home/login/forgetpassword/forgetpassword.component';
import { OtpComponent } from './home/login/otp/otp.component';
import { ChangepasswordComponent } from './home/login/changepassword/changepassword.component';


@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    HomepageComponent,
    LoginComponent,
    ForgetpasswordComponent,
    OtpComponent,
    ChangepasswordComponent,
    
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [{
    provide:HTTP_INTERCEPTORS,
    useClass:AuthorizationService,
    multi:true
  }],
  bootstrap: [AppComponent]
})
export class AppModule { }
