const exp = require('express')
var adminroutes = exp.Router()
const initDb = require('../configdb').initDb
const getDb = require('../configdb').getDb
const bodyparser = require('body-parser')
const bcrypt = require('bcrypt')
const nodemailer = require('nodemailer')
adminroutes.use(bodyparser.json())
const jwt = require('jsonwebtoken')
const secretkey = "xxxxxx"
const checkauthorization = require('../middlewares/checkauthorization')
const accountSid = 'xxxxxxxxxxxxxxxxxxxx';
const authToken = 'xxxxxxxxxxxxxxxxxxxx';
const client = require('twilio')(accountSid, authToken);




initDb()
adminroutes.post('/savestd', checkauthorization, (req, res, next) => {
  var transporter = nodemailer.createTransport({
    host: 'smtp.gmail.com',
    port: 465,
    secure: true,
    auth: {
      user: 'pk9440035@gmail.com', // Your email id
      pass: 'xxxxxxxx' // Your password
    },
    tls: {
      // do not fail on invalid certs
      rejectUnauthorized: false
    }
  });
  let info = transporter.sendMail({
    // sender address
    from: '"admin" <pk9440035@gmail.com>',
    // list of receivers
    to: req.body.gmail,
    subject: "student credetials", // Subject line
    text: `rollnumber: ${req.body.rollno}, pwd: ${req.body.password}`,
    // plain text body
    // html: "<b>Hello world?</b>" // html body
  });
  dbo = getDb();
  dbo.collection("addstudent").find({ rollno: { $eq: req.body.rollno } }).toArray((err, studentArray) => {
    if (studentArray == "") {
      bcrypt.hash(req.body.password, 6, (err, hashedpassword) => {
        if (err) {
          next(err)
        }
        else {
          req.body.password = hashedpassword

          dbo.collection("addstudent").insertOne(req.body, (err, dataArray) => {
            if (err) {
              console.log('error in saving data')
            }
            else {
              res.json({ message: "successfully added students" })
            }
          })
        }
      })

    }
    else {
      res.json({ message: "student already exists with this rollno" })
    }

  })

})

adminroutes.post('/saveatt', checkauthorization, (req, res, next) => {
  dbo = getDb()
  if (req.body == {}) {
    res.json({ message: "server did not receive data" })
  } else {
    dbo.collection("saveatt").insertOne(req.body, (err, dataArray) => {
      if (err) {
        next(err)
      }
      else {
        res.json({ message: "successfully added attendance" })
      }
    })
  }

})
adminroutes.post('/savefee', checkauthorization, (req, res, next) => {
  dbo = getDb()
  if (req.body == {}) {
    res.json({ message: "server did not receive data" })
  } else {
    dbo.collection("savefee").insertOne(req.body, (err, dataArray) => {
      if (err) {
        next(err)
      }
      else {
        res.json({ message: "successfully added fee details", data: dataArray })
      }
    })
  }

})
adminroutes.post('/savemark', checkauthorization, (req, res, next) => {
  dbo = getDb()
  if (req.body == {}) {
    res.json({ message: "server did not receive data" })
  } else {
    dbo.collection("savemark").insertOne(req.body, (err, dataArray) => {
      if (err) {
        next(err)
      }
      else {
        res.json({ message: "successfully added marks" })
      }
    })
  }

})
adminroutes.post('/savenote', checkauthorization, (req, res, next) => {
  dbo = getDb()
  if (req.body == {}) {
    res.json({ message: "server did not receive data" })
  } else {
    dbo.collection("savenote").insertOne(req.body, (err, dataArray) => {
      if (err) {
        next(err)
      }
      else {
        res.json({ message: "successfully send notifications" })
      }
    })
  }

})
adminroutes.get('/readstd', checkauthorization, (req, res, next) => {
  dbo = getDb()
  dbo.collection('addstudent').find().toArray((err, dataArray) => {
    if (err) {
      next(err)
    }
    else {
      res.json({ message: "success", data: dataArray })
    }
  })

})
adminroutes.post('/login', (req, res, next) => {
  dbo = getDb()
  dbo.collection('addstudent').find({ rollno: { $eq: req.body.rollno } }).toArray((err, userArray) => {
    if (userArray.length == 0) {
      res.json({ message: "invalid student" })
    }
    else {
      bcrypt.compare(req.body.password, userArray[0].password, (err, result) => {
        if (result == true) {
          const signedtoken = jwt.sign({ rollno: userArray[0].rollno }, secretkey, { expiresIn: "7d" })
          res.json({ message: "student login success", token: signedtoken, data: userArray })
        }
        else {
          res.json({ message: "invalid password" })
        }
      })

    }
  })
})


adminroutes.post('/adminlogin', (req, res, next) => {
  dbo = getDb()
  dbo.collection('admin').find({ rollno: { $eq: req.body.rollno } }).toArray((err, userArray) => {
    if (userArray.length == 0) {
      res.json({ message: "invalid admin" })
    }
    else if (userArray[0].password !== req.body.password) {
      res.json({ message: "invalid admin password" })
    }
    else {
      const signedtoken = jwt.sign({ rollno: userArray[0].rollno }, secretkey, { expiresIn: "7d" })
      res.json({ message: "admin login success", token: signedtoken })
    }
  })


})


adminroutes.delete('/deletemark/:rollno', checkauthorization, (req, res, next) => {
  dbo = getDb()
  dbo.collection('savemark').deleteOne({ rollno: { $eq: req.params.rollno } }, (err, success) => {
    if (err) {
      next(err)
    }
    else {
      dbo.collection('savemark').find().toArray((err, dataArray) => {
        if (err) {
          next(err)
        }
        else {
          console.log(dataArray)
          res.json({
            message: "record deleted",
            data: dataArray
          })
        }
      })
    }
  })
})
adminroutes.delete('/deleteatt/:rollno', checkauthorization, (req, res, next) => {

  dbo = getDb()
  dbo.collection('saveatt').deleteOne({ rollno: { $eq: req.params.rollno } }, (err, success) => {
    if (err) {
      next(err)
    }
    else {
      dbo.collection('saveatt').find().toArray((err, dataArray) => {
        if (err) {
          next(err)
        }
        else {
          console.log(dataArray)
          res.json({
            message: "record deleted",
            data: dataArray
          })
        }
      })
    }
  })
})
adminroutes.put('/modifyatt', checkauthorization, (req, res, next) => {
  dbo = getDb()
  dbo.collection('saveatt').updateOne({ rollno: { $eq: req.body.rollno } }, { $set: { rollno: req.body.rollno, month: req.body.month, overall: req.body.overall } }, (err, success) => {
    if (err) {
      next(err)
    }
    else {
      res.json({ message: "success" })
    }
  })
})
adminroutes.put('/modifymark', checkauthorization, (req, res, next) => {
  dbo = getDb()
  dbo.collection('savemark').updateOne({ rollno: { $eq: req.body.rollno } }, { $set: { rollno: req.body.rollno, month: req.body.month, overall: req.body.overall } }, (err, success) => {
    if (err) {
      next(err)
    }
    else {
      res.json({ message: "success" })
    }
  })
})



adminroutes.delete('/deletefee/:rollno', checkauthorization, (req, res, next) => {
  dbo = getDb()
  dbo.collection('savefee').deleteOne({ rollno: { $eq: req.params.rollno } }, (err, success) => {
    if (err) {
      next(err)
    }
    else {
      dbo.collection('savefee').find().toArray((err, dataArray) => {
        if (err) {
          next(err)
        }
        else {
          console.log(dataArray)
          res.json({
            message: "record deleted",
            data: dataArray
          })
        }
      })
    }
  })
})
adminroutes.delete('/deleteprofile/:rollno', checkauthorization, (req, res, next) => {
  dbo = getDb()
  dbo.collection('addstudent').deleteOne({ rollno: { $eq: req.params.rollno } }, (err, success) => {
    if (err) {
      next(err)
    }
    else {
      dbo.collection('addstudent').find().toArray((err, dataArray) => {
        if (err) {
          next(err)
        }
        else {

          res.json({
            message: "record deleted",
            data: dataArray
          })
        }
      })
    }
  })
})
adminroutes.post('/saveres', checkauthorization, (req, res, next) => {
  dbo = getDb()
  if (req.body == {}) {
    res.json({ message: "server did not receive data" })
  } else {
    dbo.collection("responce").insertOne(req.body, (err, dataArray) => {
      if (err) {
        next(err)
      }
      else {
        dbo.collection("request").deleteOne({ rollno: { $eq: req.body.rollno } }, (err, dataArray) => {
          if (err) {
            next(err)
          }
          else {
            res.json({ message: "send responce successfully", data: dataArray })
          }
        })
      }
    })
  }

})
adminroutes.get('/getrequest', checkauthorization, (req, res, next) => {
  dbo = getDb()
  dbo.collection('request').find().toArray((err, dataArray) => {
    if (err) {
      next(err)
    }
    else {
      res.json({ message: dataArray })
    }
  })

})
adminroutes.post('/forgetpassword', (req, res, next) => {
  console.log(req.body)
  dbo = getDb()
  dbo.collection('addstudent').find({ rollno: req.body.rollno }).toArray((err, userArray) => {
    if (err) {
      next(err)
    }
    else {
      if (userArray.length === 0) {
        res.json({ message: "user not found" })
      }
      else {

        jwt.sign({ rollno: userArray[0].rollno }, secretkey, { expiresIn: "7d" }, (err, token) => {
          if (err) {
            next(err);
          }
          else {
            var OTP = Math.floor(Math.random() * 99999) + 11111;
            console.log(OTP)

            client.messages.create({
              body: OTP,
              from: '+13343261058', // From a valid Twilio number
              to: '+91' + userArray[0].phone,  // Text this number

            })
              .then((
                message) => {
                dbo.collection('OTPCollection').insertOne({
                  OTP: OTP,
                  rollno: userArray[0].rollno,
                  OTPGeneratedTime: new Date().getTime() + 60000
                }, (err, success) => {
                  if (err) {
                    next(err)
                  }
                  else {
                    res.json({
                      "message": "user found",
                      "token": token,
                      "OTP": OTP,
                      "rollno": userArray[0].rollno
                    })
                  }
                })
              });

          }

        })
      }
    }
  })
})

//verify OTP
adminroutes.post('/verifyotp', (req, res, next) => {
  dbo = getDb()
  console.log(req.body)
  console.log(new Date().getTime())
  var currentTime = new Date().getTime()
  dbo.collection('OTPCollection').find({ OTP: req.body.OTP }).toArray((err, OTPArray) => {
    if (err) {
      next(err)
    }
    else if (OTPArray.length === 0) {
      res.json({ "message": "invalidOTP" })
    }
    else if (OTPArray[0].OTPGeneratedTime < currentTime) {
      res.json({ "message": "OTP expired" })
    }
    else {

      dbo.collection('OTPCollection').deleteOne({ OTP: req.body.OTP }, (err, success) => {
        if (err) {
          next(err);
        }
        else {
          console.log(OTPArray)
          res.json({ "message": "verifiedOTP" })
        }
      })
    }
  })
})

//changing password
adminroutes.put('/changepassword', (req, res, next) => {
  dbo = getDb()
  console.log(req.body)
  bcrypt.hash(req.body.password, 6, (err, hashedPassword) => {
    if (err) {
      next(err)
    } else {
      console.log(hashedPassword)
      dbo.collection('addstudent').updateOne({ rollno: req.body.rollno }, {
        $set: {
          password: hashedPassword
        }
      }, (err, success) => {
        if (err) {
          next(err)
        }
        else {
          res.json({ "message": "password changed" })
        }
      })
    }
  })

})








adminroutes.use((err, req, res, next) => {
  console.log(err)
})

module.exports = adminroutes