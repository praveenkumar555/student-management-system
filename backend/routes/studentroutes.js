const exp = require('express')
var studentroutes = exp.Router()
const initDb = require('../configdb').initDb
const getDb = require('../configdb').getDb
const bodyparser = require('body-parser')
studentroutes.use(bodyparser.json())
const secretkey = "secret"
const checkauthorization = require('../middlewares/checkauthorization')

initDb()
studentroutes.get('/readatt', checkauthorization, (req, res) => {
  dbo = getDb()
  dbo.collection('saveatt').find().toArray((err, dataArray) => {
    if (err) {
      console.log("error in reading in data")
    }
    else {
      res.json({ message: dataArray })
    }
  })

})
studentroutes.post('/viewspecificattendance', checkauthorization, (req, res) => {
  dbo = getDb()
  dbo.collection('saveatt').find({ rollno: { $eq: req.body.rollno } }).toArray((err, dataArray) => {
    if (err) {
      console.log("error in reading in data")
    }
    else {
      res.json({ message: dataArray })
    }
  })

})
studentroutes.get('/readfee', checkauthorization, (req, res) => {
  dbo = getDb()
  dbo.collection('savefee').find().toArray((err, dataArray) => {
    if (err) {
      console.log("error in reading in data")
    }
    else {
      res.json({ message: dataArray })
    }
  })
})
studentroutes.post('/viewspecificfeestatus', checkauthorization, (req, res) => {
  dbo = getDb()
  dbo.collection('savefee').find({ rollno: { $eq: req.body.rollno } }).toArray((err, dataArray) => {
    if (err) {
      console.log("error in reading in data")
    }
    else {
      res.json({ message: dataArray })
    }
  })
})

studentroutes.get('/readmark', checkauthorization, (req, res) => {
  dbo = getDb()
  dbo.collection('savemark').find().toArray((err, dataArray) => {
    if (err) {
      console.log("error in reading in data")
    }
    else {
      res.json({ message: dataArray })
    }
  })

})
studentroutes.post('/viewSpecificmarks', checkauthorization, (req, res) => {
  dbo = getDb()
  dbo.collection('savemark').find({ rollno: { $eq: req.body.rollno } }).toArray((err, dataArray) => {
    if (err) {
      console.log("error in reading in data")
    }
    else {
      res.json({ message: dataArray })
    }
  })

})
studentroutes.get('/readnote', checkauthorization, (req, res) => {
  dbo = getDb()
  dbo.collection('savenote').find().toArray((err, dataArray) => {
    if (err) {
      console.log("error in reading in data")
    }
    else {
      res.json({ message: dataArray })
    }
  })

})

studentroutes.post('/saverequest', checkauthorization, (req, res) => {
  dbo = getDb()
  if (req.body == {}) {
    res.json({ message: "server did not receive data" })
  } else {
    dbo.collection("request").insertOne(req.body, (err, dataArray) => {
      if (err) {
        console.log('error in saving data')
      }
      else {
        //res.json({message:"successfully applied for leave"})
        dbo.collection("responce").deleteOne({ rollno: { $eq: req.body.rollno } }, (err, dataArray) => {
          if (err) {
            next(err)
          }
          else {
            res.json({ message: "successfully applied for leave", data: dataArray })
          }
        })
      }
    })
  }

})

studentroutes.post('/viewspecificstudentdetails', checkauthorization, (req, res, next) => {
  dbo = getDb()
  dbo.collection('addstudent').find({ rollno: { $eq: req.body.rollno } }).toArray((err, dataArray) => {
    if (err) {
      next(err)
    }
    else {
      res.json({ message: dataArray })
    }
  })

})
studentroutes.post('/viewspecificrequest', checkauthorization, (req, res, next) => {
  dbo = getDb()
  dbo.collection('responce').find({ rollno: { $eq: req.body.rollno } }).toArray((err, dataArray) => {
    if (err) {
      next(err)
    }
    else {
      res.json({ message: dataArray })
    }
  })

})





studentroutes.use((err, req, res, next) => {
  console.log(err)
})

module.exports = studentroutes